# EL1211 - Metode Numerik dan Teknik Komputasi
## Teknik Elektro - Institut Teknologi Kalimantan

### Content: 

- ets           : soal ets
- homework      : soal tugas
- quiz          : soal quiz
- references    : ebook literatur
- slides        : slide presentasi kuliah
- solution      : solusi soal, dll
